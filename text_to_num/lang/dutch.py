# MIT License

# Copyright (c) 2018-2019 Groupe Allo-Media

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from typing import Dict, Optional, Set, Tuple, List

from .base import Language

#
# CONSTANTS
# Built once on import.
#

# Those words multiplies lesser numbers (see Rules)
# Special case: "hundred" is processed apart.
MULTIPLIERS = {
    "duizend": 1_000,
    "duizenden": 1_000,
    "miljoen": 1_000_000,
    "miljoenen": 1_000_000,
    "miljard": 1_000_000_000,
    "miljarden": 1_000_000_000,
    "biljoen": 1_000_000_000_000,
    "biljoenen": 1_000_000_000_000,
}


# Units are terminals (see Rules)
# Special case: "zero/O" is processed apart.
UNITS: Dict[str, int] = {
    word: value
    for value, word in enumerate(
        "een twee drie vier vijf zes zeven acht negen".split(), 1
    )
}

# Single tens are terminals (see Rules)
STENS: Dict[str, int] = {
    word: value
    for value, word in enumerate(
        "tien elf twaalf dertien veertien vijftien zestien zeventien achttien negentien".split(),
        10,
    )
}

# Ten multiples
# Ten multiples may be followed by a unit only;
MTENS: Dict[str, int] = {
    word: value * 10
    for value, word in enumerate(
        "twintig dertig veertig vijftig zestig zeventig tachtig negentig".split(), 2
    )
}

# Ten multiples that can be combined with STENS
MTENS_WSTENS: Set[str] = set()


# "hundred" has a special status (see Rules)
HUNDRED = {"honderd": 100, "honderden": 100}


# Composites are tens already composed with terminals in one word.
# Composites are terminals.

COMPOSITES: Dict[str, int] = {
    ("ën" if unit_word[-1] == 'e' else "en").join((unit_word, ten_word)): ten_val + unit_val
    for ten_word, ten_val in MTENS.items()
    for unit_word, unit_val in UNITS.items()
}

COMPOSITES_HUNDRED: Dict[str, int] = {
    "".join((unit_word, hundred_word)): unit_val * hundred_val
    for unit_word, unit_val in UNITS.items()
    for hundred_word, hundred_val in HUNDRED.items()
}

COMPOSITES_STENS_HUNDRED: Dict[str, int] = {
    "".join((unit_word, hundred_word)): unit_val * hundred_val
    for unit_word, unit_val in STENS.items()
    for hundred_word, hundred_val in HUNDRED.items()
}

COMPOSITES_THOUSAND: Dict[str, int] = {
    "".join((unit_word, thousand_word)): unit_val * thousand_val
    for unit_word, unit_val in UNITS.items()
    for thousand_word, thousand_val in [("duizend", 1000)]
}

COMPOSITES_STENS_THOUSAND: Dict[str, int] = {
    "".join((unit_word, thousand_word)): unit_val * thousand_val
    for unit_word, unit_val in STENS.items()
    for thousand_word, thousand_val in [("duizend", 1000)]
}

HUNDRED.update(COMPOSITES_HUNDRED)
HUNDRED.update(COMPOSITES_STENS_HUNDRED)

COMPOSITES.update(COMPOSITES_HUNDRED)
COMPOSITES.update(COMPOSITES_THOUSAND)
COMPOSITES.update(COMPOSITES_STENS_HUNDRED)
COMPOSITES.update(COMPOSITES_STENS_THOUSAND)

MULTIPLIERS.update(COMPOSITES_THOUSAND)
MULTIPLIERS.update(COMPOSITES_STENS_THOUSAND)

# All number words

NUMBERS = MULTIPLIERS.copy()
NUMBERS.update(UNITS)
NUMBERS.update(STENS)
NUMBERS.update(MTENS)
NUMBERS.update(HUNDRED)
NUMBERS.update(COMPOSITES)


class Dutch(Language):

    ADDITIONS: Dict[str, int] = {}
    MULTIPLIERS = MULTIPLIERS
    UNITS = UNITS
    STENS = STENS
    MTENS = MTENS
    MTENS_WSTENS = MTENS_WSTENS
    HUNDRED = HUNDRED
    NUMBERS = NUMBERS

    SIGN = {"plus": "+", "minus": "-"}
    ZERO = {"nul", "o"}
    DECIMAL_SEP = "punt"
    DECIMAL_SYM = "."

    AND_NUMS: Set[str] = set()
    AND = "en"
    NEVER_IF_ALONE = {"een"}

    # Relaxed composed numbers (two-words only)
    # start => (next, target)
    RELAXED: Dict[str, Tuple[str, str]] = {}

    def ord2card(self, word: str) -> Optional[str]:
        """Convert ordinal number to cardinal.

        Return None if word is not an ordinal or is better left in letters
        as is the case for fist and second.
        """

        if word.endswith("de"):
            source = word[:-2]
        elif word.endswith("eerste"):
            source = word.replace("eerste", "een")
        elif word.endswith("ste"):
            source = word[:-3]
        else:
            return None

        if source not in self.NUMBERS:
            return None

        return source

    def num_ord(self, digits: str, original_word: str) -> str:
        """Add suffix to number in digits to make an ordinal"""
        if (original_word.endswith("de") or original_word.endswith("te")):
            return f"{digits}e"
        return digits

    def normalize(self, word: str) -> str:
        return word

    def tokenize(self, sent: str) -> List[str]:
        "Split word with honderd"
        items = sent.split()
        for i, item in enumerate(items):
            if "honderd" in item:
                parts = item.split("honderd")
                if len(parts) == 2 and parts[-1] != "ste" and parts[-1] != "en" and parts[-1] != "jes":
                    items[i] = "honderd ".join(parts)
        return " ".join(items).split()
