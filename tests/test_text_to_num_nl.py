# MIT License

# Copyright (c) 2018-2019 Groupe Allo-Media

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


"""
Test the ``text_to_num`` library.
"""
from typing import Union
from unittest import TestCase
from text_to_num import alpha2digit


class TestTextToNumNL(TestCase):
    def assertNumber(self, input_string: str, expected: Union[str, int]) -> None:
        if(isinstance(expected, int)):
            expected = str(expected)
        output = alpha2digit(input_string, 'nl')
        self.assertEqual(expected, output)

    def test_alpha2digit_formal(self) -> None:

        source = "in de zeventiende eeuw hadden we vijfenvijftig fouten"
        expected = "in de 17e eeuw hadden we 55 fouten"
        self.assertEqual(alpha2digit(source, "nl"), expected)

        source = "tweeëndertig"
        expected = "32"
        self.assertEqual(alpha2digit(source, "nl"), expected)

        source = "volgens advocaat honderdjes"
        expected = "volgens advocaat honderdjes"
        self.assertEqual(alpha2digit(source, "nl"), expected)

    def test_zero_to_nine(self) -> None:
        self.assertNumber('nul', 0)
        self.assertNumber('een', 1)
        self.assertNumber('twee', 2)
        self.assertNumber('drie', 3)
        self.assertNumber('vier', 4)
        self.assertNumber('vijf', 5)
        self.assertNumber('zes', 6)
        self.assertNumber('zeven', 7)
        self.assertNumber('acht', 8)
        self.assertNumber('negen', 9)

    def test_11_to_19(self) -> None:
        self.assertNumber('elf', 11)
        self.assertNumber('twaalf', 12)
        self.assertNumber('dertien', 13)
        self.assertNumber('veertien', 14)
        self.assertNumber('vijftien', 15)
        self.assertNumber('zestien', 16)
        self.assertNumber('zeventien', 17)
        self.assertNumber('achttien', 18)
        self.assertNumber('negentien', 19)

    def test_hundreds(self) -> None:
        self.assertNumber('honderd', 100)
        self.assertNumber('tweehonderd', 200)
        self.assertNumber('driehonderd', 300)
        self.assertNumber('vierhonderd', 400)
        self.assertNumber('vijfhonderd', 500)
        self.assertNumber('zeshonderd', 600)
        self.assertNumber('zevenhonderd', 700)
        self.assertNumber('achthonderd', 800)
        self.assertNumber('negenhonderd', 900)
        self.assertNumber('elfhonderd', 1100)
        self.assertNumber('twaalfhonderd', 1200)
        self.assertNumber('dertienhonderd', 1300)
        self.assertNumber('veertienhonderd', 1400)

    def test_more_complex_numbers(self) -> None:
        self.assertNumber('vijf duizend', '5000')
        self.assertNumber('vijf en twintig', '5 en 20')
        self.assertNumber('twintig duizend', '20000')
        self.assertNumber('vijf en twintig duizend', '5 en 20000')
        self.assertNumber('vijfentwintig duizend', '25000')
        self.assertNumber('dertienhonderd tweeëntwintig', '1322')
        self.assertNumber('duizend driehonderd tweeëntwintig', '1322')
        self.assertNumber('tweehonderd', '200')
        self.assertNumber('negentienhonderd vierentachtig', '1984')
        self.assertNumber('negentien vierentachtig', '19 84')
        self.assertNumber('zesenzestig', '66')
        self.assertNumber('elf december tweeduizend negentien',
                          '11 december 2019')
        self.assertNumber('tweeduizend dertien', '2013')

    def test_ord_to_card(self) -> None:
        self.assertNumber("eerste", "eerste")
        self.assertNumber("tweede", "tweede")
        self.assertNumber("derde", "derde")
        self.assertNumber("negenennegentig", 99)
        self.assertNumber("negenennegentigste", "99e")
        self.assertNumber("honderdnegenennegentig", "199")
        self.assertNumber("honderdnegenennegentigste", "199e")
        self.assertNumber("honderdste", "100e")

    def test_twintigste_eeuw(self) -> None:
        source = "in de twintigste eeuw"
        expected = "in de 20e eeuw"

        self.assertEqual(alpha2digit(source, "nl", True), expected)

    def test_eenentwintigste_eeuw(self) -> None:
        source = "in de eenentwintigste eeuw"
        expected = "in de 21e eeuw"

        self.assertEqual(alpha2digit(source, "nl", True), expected)

    def test_capitalized(self) -> None:
        source = "Eerste week was wel spannend"
        expected = "Eerste week was wel spannend"
        self.assertEqual(alpha2digit(source, "nl", True), expected)
