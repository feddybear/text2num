# MIT License

# Copyright (c) 2018-2019 Groupe Allo-Media

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


"""
Test the ``text_to_num`` library.
"""
from unittest import TestCase
from text_to_num import alpha2digit, text2num


class TestTextToNumAr(TestCase):
    def test_alpha2digit_formal(self):
        source = "لدي خمسة عشر تفاحة"
        expected = "لدي 15 تفاحة"
        self.assertEqual(alpha2digit(source, "ar"), expected)

        source = "ناقص خمس درجات"
        expected = "-5 درجات"
        self.assertEqual(alpha2digit(source, "ar"), expected)

        source = "اثنان وسبعون خيل"
        expected = "72 خيل"
        self.assertEqual(alpha2digit(source, "ar"), expected)

        source = "أكثر من ثلاثة آلاف منزل"
        expected = "أكثر من 3000 منزل"
        self.assertEqual(alpha2digit(source, "ar"), expected)

        source = "ما يقرب من ستمائة وخمسة وسبعين دولارًا"
        expected = "ما يقرب من 675 دولارًا"
        self.assertEqual(alpha2digit(source, "ar"), expected)
