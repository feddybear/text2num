# MIT License

# Copyright (c) 2018-2019 Groupe Allo-Media

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


"""
Test the ``text_to_num`` library.
"""
from unittest import TestCase
from text_to_num import alpha2digit, text2num


class TestTextToNumFl(TestCase):
    def test_alpha2digit_formal(self):
        source = "mayroon akong labing limang mansanas"
        expected = "mayroon akong 15 mansanas"
        self.assertEqual(alpha2digit(source, "fl"), expected)

        source = "ito ang aking dalawampu't unang pagtatangka"
        expected = "ito ang aking 21 pagtatangka"
        self.assertEqual(alpha2digit(source, "fl"), expected)

        source = "daang pitumpu't limang dolyar"
        expected = "175 dolyar"
        self.assertEqual(alpha2digit(source, "fl"), expected)

        source = "nakilala ko ang tatlong libo pitumpu't anim na tao"
        expected = "nakilala ko ang 3076 na tao"
        self.assertEqual(alpha2digit(source, "fl"), expected)
